# RFID-powerswitch
## Strom gibt's nur, wenn du den richtigen Schlüssel hast und nicht vor die Wand gefahren bist.

Der Sketch schaltet die Batterie von der Hauptschaltung und damit auch die Motoren an und aus.
Dafür wird ein RFID-Reader so konfiguriert, dass dieser nur einen passendes RFID-tag akzeptiert.
Zusätzlich wird ein mechanischer Endtaster ausgewertet, der die Versorgung bei einer Kollision mit einer Wand trennt und so einen Not-Stop realisiert.

Bei der Entwicklung wurde eine Bibliothek eingebunden, um den Reader anzusteuern.
[https://github.com/miguelbalboa/rfid]
(https://github.com/miguelbalboa/rfid, "MFRC522")
Diese ist direkt mit der Arduino IDE installierbar.