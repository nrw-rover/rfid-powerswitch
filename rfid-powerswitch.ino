#include <SPI.h>        // RC522 Module uses SPI protocol
#include <MFRC522.h>    // Library for Mifare RC522 Devices


 /* * Pin layout should be as follows: */
 /* * Signal     Pin              Pin               Pin */
 /* *            Arduino Uno      Arduino Mega      MFRC522 board */
 /* * ------------------------------------------------------------ */
 /* * Reset      9                5                 RST */
 /* * SPI SS     10               53                SDA */
 /* * SPI MOSI   11               51                MOSI */
 /* * SPI MISO   12               50                MISO */
 /* * SPI SCK    13               52                SCK */
 /* * */


#define MFR_RST_PIN 9
#define MFR_SS_PIN 10

#define ONOFF_SWITCH 8
#define KILL_SWITCH 7


int isLocked = 0;


MFRC522 mfrc522(MFR_SS_PIN, MFR_RST_PIN);
byte readCard[4]; 
byte correctCard[4] = {0xF6, 0xE5, 0x0A, 0x2E};

void setup(){
  pinMode(KILL_SWITCH, INPUT);
  pinMode(ONOFF_SWITCH, OUTPUT);
  digitalWrite(ONOFF_SWITCH, LOW);
  Serial.begin(9600);
  SPI.begin();           // MFRC522 Hardware uses SPI protocol
  mfrc522.PCD_Init();    // Initialize MFRC522 Hardware

  
  mfrc522.PCD_SetAntennaGain(mfrc522.RxGain_max);

  Serial.println(F("RFID-POWERSWITCH"));
  ShowReaderDetails();  // Show details of PCD - MFRC522 Card Reader details
}


void loop(){
  if(digitalRead(KILL_SWITCH)){
    isLocked = 1;
    digitalWrite(ONOFF_SWITCH, isLocked);
    Serial.println("KILLED BY KILLSWITCH");
  }
  if(getID()==1){
    boolean unlocked = true;
    for (int i = 0; i<4; i++){
      if(readCard[i] != correctCard[i]){
    	unlocked = false;
      }
    }
    if(unlocked){
      for (int i = 0; i<4; i++){
	readCard[i] = 0;
      }
      Serial.println("CORRECT RFID-TAG!");
      isLocked ^= 1;
      digitalWrite(ONOFF_SWITCH, isLocked);
    }
  }
} 


uint8_t getID() {
  // Getting ready for Reading PICCs
  if ( ! mfrc522.PICC_IsNewCardPresent()) { //If a new PICC placed to RFID reader continue
    return 0;
  }
  // Serial.println("RFID TAG present!");
  if ( ! mfrc522.PICC_ReadCardSerial()) {   //Since a PICC placed get Serial and continue
    return 0;
  }
  //Serial.println(F("RFID TAG scanned!"));
  // There are Mifare PICCs which have 4 byte or 7 byte UID care if you use 7 byte PICC
  // I think we should assume every PICC as they have 4 byte UID
  // Until we support 7 byte PICCs
  Serial.println(F("Scanned PICC's UID:"));
  for ( uint8_t i = 0; i < 4; i++) {  //
    readCard[i] = mfrc522.uid.uidByte[i];
    Serial.print(readCard[i], HEX);
    Serial.print(" ");
  }
  Serial.println("");
  mfrc522.PICC_HaltA(); // Stop reading
  return 1;
}


void ShowReaderDetails() {
  // Get the MFRC522 software version
  byte v = mfrc522.PCD_ReadRegister(mfrc522.VersionReg);
  Serial.print(F("MFRC522 Software Version: 0x"));
  Serial.print(v, HEX);
  if (v == 0x91)
    Serial.print(F(" = v1.0"));
  else if (v == 0x92)
    Serial.print(F(" = v2.0"));
  else
    Serial.print(F(" (unknown),probably a chinese clone?"));
  Serial.println("");
  // When 0x00 or 0xFF is returned, communication probably failed
  if ((v == 0x00) || (v == 0xFF)) {
    Serial.println(F("WARNING: Communication failure, is the MFRC522 properly connected?"));
    Serial.println(F("SYSTEM HALTED: Check connections."));
    // Visualize system is halted
    while (true); // do not go further
  }
}


